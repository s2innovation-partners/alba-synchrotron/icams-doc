# IC@MS setup instruction


## Prerequirements
- IP Address of TANGO_HOST
- docker (19.03.2), docker-compose (1.29.1) installed

## Installation instructions

1. Export the following env's, filling with an appropriate values before:

```
export MONGO_ADMIN_PASSWORD=access4icams &&
export MONGO_ADMIN_USER=admin &&
export MONGO_PASSWORD=access4icams &&
export MONGO_USER=icams &&
export LOCAL_TANGO_ARCHIVING=ARCHIVING_IP:3306
```

remember about setting `TANGO_HOST` to the desired remote IP.

2. Run the docker compose file present within the folder:

```
docker-compose up
```

All the containers should be fetched automatically from gitlab registry
